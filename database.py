from sqlalchemy import Column, String, Integer, PickleType, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'
    
    id = Column(Integer, primary_key=True)
    email = Column(String(100), unique=True)
    password = Column(String(32))

    def __init__(self, email, password):
        self.email = email
        self.password = password

    def __repr__(self):
        return '<User {email}>'.format(email=self.email)    

class Note(Base):
    __tablename__ = 'notes'
    
    id = Column(Integer, primary_key=True)
    title = Column(String(100), unique=True)
    text = Column(Text())
    user_id = Column(Integer)

    def __init__(self, title, text, user_id):
        self.title = title
        self.text = text
        self.user_id = user_id

    def __repr__(self):
        return '<Note {title}>'.format(title=self.title) 


# creating a standard sql alchemy engine. SQLite is just perfect
# for our needs
db_path = 'sqlite:///database.sqlite'
engine = create_engine(db_path)
Base.metadata.create_all(engine)
