from functools import wraps
from sqlalchemy import or_
from markdown import markdown
from database import User, Note, Base
from flask import Flask, render_template, request, url_for, redirect, session



import sqlalchemy.exc
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from database import db_path

engine = create_engine(db_path)
Base.metadata.bind = engine
session_class = sessionmaker(bind=engine)
DBSession = session_class()



def requireLogin(f):
    # Проверяем логин
    @wraps(f)
    def checker(*args, **kwargs):
        if 'user_id' not in session:
            session.clear()
            return redirect(url_for('index'))
        return f(*args, **kwargs)
    return checker


@app.route('/')
def index(data=None):
    return render_template('items/login.html', data=data)


@app.route('/login', methods=['POST'])
def login():
    data = dict()
    if (request.form['action'] and
            request.form['email'] and
            request.form['password']):
        data['email'] = request.form['email']
        data['password'] = request.form['password']
        if request.form['action'] == 'signup':
            
            # чекаем пользователя
            my_user = DBSession.query(User).filter_by(
                email=data['email']).first()
            if not my_user:
                # если нет его, то создаем
                my_user = User(data['email'], data['password'])
                DBSession.add(my_user)
                try:
                    DBSession.commit()
                except:
                    DBSession.rollback()
            else:
                data['error'] = 'register'

        # проверяем существование пользователя
        if 'error' not in data:
            my_user = DBSession.query(User).filter_by(
                email=data['email'], password=data['password']).first()
            if my_user:
                # создаем новую пользовательскую сессию
                session['user_id'] = my_user.id
                session['user_email'] = my_user.email
                return redirect(url_for('dashboard'))
            else:
                data['error'] = 'login'

    return index(data)


@app.route('/logout')
@requireLogin
def logout():
    # Clear sessions
    session.clear()

    return redirect(url_for('index'))


@app.route('/dashboard')
@requireLogin
def dashboard(my_param_note=None):
    my_main_note = None

    # ищем заметке
    my_notes = my_param_note
    if not my_notes:
        my_notes = DBSession.query(Note).filter_by(
            user_id=session['user_id']).order_by(Note.id.desc()).all()
    if my_notes:
        # показываем первый результат
        my_main_note = my_notes[0]

    # показываем первую заметку
    if request.args.get('id'):
        my_note_temp = DBSession.query(Note).filter_by(id=request.args.get('id')).first()
        if my_note_temp:
            my_main_note = my_note_temp

    # Передаем в шаблон
    data = dict()
    data['notes'] = my_notes
    data['main_note'] = my_main_note
    data['markdown'] = markdown

    return render_template('items/dashboard.html', data=data)


@app.route('/search')
@requireLogin
def search():
    #q = request.args.get('q')
    
    #return dashboaqueryrd(DBSession.query(Note).filter(
        #or_(Note.title.like('%' + q + '%'), Note.text.like('%' + q + '%')
            #)).filter_by(user_id=session['user_id']).order_by(
            #Note.id.desc()).all())
    return redirect('/')


@app.route('/new')
@requireLogin
def new():
    return render_template('items/new.html')


@app.route('/new/save', methods=['POST'])
@requireLogin
def save_note():
    myNote = Note(request.form['title'], request.form[
                  'text'], session['user_id'])
    DBSession.add(myNote)
    try:
        DBSession.commit()
    except:
        DBSession.rollback()

    return redirect(url_for('dashboard'))


@app.route('/edit')
@requireLogin
def edit(data=None):
    id = request.args.get('id')
    my_note = Note.query.filter_by(id=id).first()
    data = dict()
    data['main_note'] = my_note
    return render_template('items/edit.html', data=data)


@app.route('/edit_note', methods=['POST'])
@requireLogin
def edit_note(data=None):
    if request.form['id']:
        
        # заносим заметку в базу данных
        my_note = Note.query.filter_by(id=request.form['id']).first()
        my_note.title = request.form['title']
        my_note.text = request.form['text']
        try:
            DBSession.commit()
        except:
            DBSession.rollback()

    return redirect(url_for('dashboard'))


@app.route('/delete')
@requireLogin
def remove():
    id = request.args.get('id')
    my_note = Note.query.filter_by(id=id).first()
    data = dict()
    data['main_note'] = my_note
    data['markdown'] = markdown

    return render_template('items/delete.html', data=data)


@app.route('/delete_note')
@requireLogin
def remove_note():
    id = request.args.get('id')
    # Delete
    my_note = Note.query.filter_by(id=id).first()
    DBSession.delete(my_note)
    try:
        DBSession.commit()
    except:
        DBSession.rollback()

    return redirect(url_for('dashboard', id=id))

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
    print('ok')
